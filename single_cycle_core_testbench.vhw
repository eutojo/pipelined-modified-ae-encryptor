--------------------------------------------------------------------------------
-- Copyright (c) 1995-2003 Xilinx, Inc.
-- All Right Reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 7.1.03i
--  \   \         Application : ISE WebPACK
--  /   /         Filename : single_cycle_core_testbench.vhw
-- /___/   /\     Timestamp : Tue Jul 25 16:23:28 2006
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: 
--Design Name: single_cycle_core_testbench
--Device: Xilinx
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE STD.TEXTIO.ALL;
use ieee.numeric_std.all ; 

ENTITY single_cycle_core_testbench IS
END single_cycle_core_testbench;

ARCHITECTURE testbench_arch OF single_cycle_core_testbench IS
    FILE RESULTS: TEXT OPEN WRITE_MODE IS "results.txt";

    COMPONENT single_cycle_core
        PORT (
            reset : In std_logic;
            clk : In std_logic;
				input_out : In std_logic_vector(7 downto 0);
				ls_addr : in std_logic_vector(3 downto 0);
				file_flag : out std_logic
        );
    END COMPONENT;

    SIGNAL reset : std_logic := '1';
    SIGNAL clk : std_logic := '0';
	 SIGNAL input_out : STD_LOGIC_VECTOR(7 downto 0) := "00000000";----------
	 SIGNAL file_flag : std_logic;
	 SIGNAL ls_addr : std_logic_vector(3 downto 0) := (OTHERS => '0');
	 
    SHARED VARIABLE TX_ERROR : INTEGER := 0;
    SHARED VARIABLE TX_OUT : LINE;

    constant PERIOD : time := 200 ns;
    constant DUTY_CYCLE : real := 0.5;
    constant OFFSET : time := 0 ns;


    BEGIN
        UUT : single_cycle_core
        PORT MAP (
            reset => reset,
            clk => clk,
				input_out => input_out,
				ls_addr => ls_addr,
				file_flag => file_flag
        );

        PROCESS    -- clock process for clk
        BEGIN
            WAIT for OFFSET;
            CLOCK_LOOP : LOOP
                clk <= '0';
                WAIT FOR (PERIOD - (PERIOD * DUTY_CYCLE));
                clk <= '1';
                WAIT FOR (PERIOD * DUTY_CYCLE);
            END LOOP CLOCK_LOOP;
        END PROCESS;

        PROCESS
            BEGIN
                -- -------------  Current Time:  285ns
                WAIT FOR 285 ns;
                reset <= '0';
                -- -------------------------------------
                WAIT FOR 35000 ns;--3175 ns;

                IF (TX_ERROR = 0) THEN
                    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
                    STD.TEXTIO.writeline(RESULTS, TX_OUT);
                    ASSERT (FALSE) REPORT
                      "Simulation successful (not a failure).  No problems detected."
                      SEVERITY FAILURE;
                ELSE
                    STD.TEXTIO.write(TX_OUT, TX_ERROR);
                    STD.TEXTIO.write(TX_OUT,
                        string'(" errors found in simulation"));
                    STD.TEXTIO.writeline(RESULTS, TX_OUT);
                    ASSERT (FALSE) REPORT "Errors found during simulation"
                         SEVERITY FAILURE;
                END IF;
            END PROCESS;
				

				-- read string to encrypt --
				PROCESS
            FILE INPUT_FILE: TEXT OPEN READ_MODE IS "input.txt";
            VARIABLE input_line : LINE;
            VARIABLE input_in : CHARACTER;
            
            BEGIN
	
				WAIT UNTIL file_flag = '1' AND file_flag'EVENT;
					ls_addr <= ls_addr + 1;
					IF ENDFILE(INPUT_FILE) THEN
						input_out <= "00000000";
					ELSE
						READLINE(INPUT_FILE, input_line);
						READ(input_line, input_in);
						input_out <= std_logic_vector(to_unsigned(natural(character'pos(input_in)), 8));
					END IF;
					
            END PROCESS;
							



    END testbench_arch;

