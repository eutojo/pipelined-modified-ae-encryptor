----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:47:29 04/21/2018 
-- Design Name: 
-- Module Name:    ID_EX - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ID_EX is
	port( alu_src				: in std_logic;
			mem_write			: in std_logic;
			mem_to_reg			: in std_logic;
			mem_read				: in std_logic;
			alu_ctr				: in std_logic_vector(2 downto 0);
			reg_write 			: in std_logic;
			read_data_a 		: in std_logic_vector(15 downto 0); --from register
			read_data_b			: in std_logic_vector(15 downto 0); --from register
			sign_extend			: in std_logic_vector(15 downto 0);
			insn_Rs				: in std_logic_vector(3 downto 0); -- Rs
			insn_Rt				: in std_logic_vector(3 downto 0); -- Rt
			insn_Rd				: in std_logic_vector(3 downto 0); -- Rd
			next_pc				: in std_logic_vector(4 downto 0); 
			stall					: in std_logic;
			clk					: in std_logic;
			--zero_to_alu     : in std_logic;
			
			
			----- out to EX stage
			alu_src_out			: out std_logic;
			mem_write_out		: out std_logic;
			mem_to_reg_out		: out std_logic;
			mem_read_out		: out std_logic;
			alu_ctr_out			: out std_logic_vector(2 downto 0);
			reg_write_out 		: out std_logic;
			read_data_a_out 	: out std_logic_vector(15 downto 0);
			read_data_b_out	: out std_logic_vector(15 downto 0);
			sign_extend_out	: out std_logic_vector(15 downto 0);
			next_pc_out			: out std_logic_vector(4 downto 0); 
			insn_Rs_out			: out std_logic_vector(3 downto 0); -- Rs
			insn_Rt_out			: out std_logic_vector(3 downto 0); -- Rt
			insn_Rd_out			: out std_logic_vector(3 downto 0)); -- Rd
end ID_EX;

architecture Behavioral of ID_EX is
begin
	process
		begin
			if rising_edge(clk) then 
				if stall = '0' then -- not stall, pass all signal to next stage
					alu_src_out <= alu_src;
					mem_write_out <= mem_write;
					mem_to_reg_out <= mem_to_reg;
					mem_read_out <= mem_read;
					alu_ctr_out <= alu_ctr;
					reg_write_out <= reg_write;
					read_data_a_out <= read_data_a;
					read_data_b_out <= read_data_b;
					sign_extend_out <= sign_extend;
					next_pc_out <= next_pc;
					insn_Rs_out <= insn_Rs;
					insn_Rt_out <= insn_Rt;
					insn_Rd_out <= insn_Rt;
				end if;
			end if;
		end process;		
end Behavioral;

