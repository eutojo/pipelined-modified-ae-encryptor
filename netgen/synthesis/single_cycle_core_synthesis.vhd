--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.87xd
--  \   \         Application: netgen
--  /   /         Filename: .vhd
-- /___/   /\     Timestamp: Tue Mar 20 19:09:48 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm single_cycle_core -w -dir netgen/synthesis -ofmt vhdl -sim single_cycle_core.ngc single_cycle_core_synthesis.vhd 
-- Device	: xc4vlx15-12-sf363
-- Input file	: single_cycle_core.ngc
-- Output file	: C:\Users\Eunike\Downloads\single_cycle_core\netgen\synthesis\single_cycle_core_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: single_cycle_core
-- Xilinx	: C:\Xilinx\13.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

