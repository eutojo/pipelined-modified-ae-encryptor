--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.87xd
--  \   \         Application: netgen
--  /   /         Filename: single_cycle_core_translate.vhd
-- /___/   /\     Timestamp: Mon Apr 23 00:15:23 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -tpw 0 -ar Structure -tm single_cycle_core -w -dir netgen/translate -ofmt vhdl -sim single_cycle_core.ngd single_cycle_core_translate.vhd 
-- Device	: 4vlx15sf363-12
-- Input file	: single_cycle_core.ngd
-- Output file	: C:\Users\Monica\Documents\Uni\COMP\eutojo-comp3211-project-a-2be6f439fcd3\netgen\translate\single_cycle_core_translate.vhd
-- # of Entities	: 1
-- Design Name	: single_cycle_core
-- Xilinx	: C:\Xilinx\13.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity single_cycle_core is
  port (
    clk : in STD_LOGIC := 'X'; 
    reset : in STD_LOGIC := 'X' 
  );
end single_cycle_core;

architecture Structure of single_cycle_core is
begin
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

