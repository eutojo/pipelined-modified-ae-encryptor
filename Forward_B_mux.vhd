----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:46:30 04/24/2018 
-- Design Name: 
-- Module Name:    Foward_A_mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Foward_B_mux is
    port ( mux_select : in  std_logic(1 downto 0);
           data_a     : in  std_logic(15 downto 0);
           data_b     : in  std_logic(15 downto 0);
			  data_c		 : in  std_logic(15 downto 0);
           data_out   : out std_logic (15 downto 0));
end Foward_A_mux;

architecture structural of Foward_B_mux is

component mux_3to1_1b is
    port ( mux_select : in  std_logic(1 downto 0);
           data_a     : in  std_logic;
           data_b     : in  std_logic;
			  data_c		 : in  std_logic;
           data_out   : out std_logic);
end component;

begin
	-- this for-generate-loop replicates 16 single-bit 3-to-1 mux
	muxes : for i in 15 downto 0 generate
		bit_mux : mux_3to1_1b
		port map ( mux_select => mux_select,
					  data_a => data_a(i),
					  data_b => data_b(i),
					  data_c => data_c(i),
					  data_out => data_out(i) );
	end generate muxes;				  
	
end Behavioral;

