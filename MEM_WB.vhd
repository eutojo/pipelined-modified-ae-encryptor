----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:32:05 04/23/2018 
-- Design Name: 
-- Module Name:    MEM_WB - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MEM_WB is
	port( mem_to_reg 			: in std_logic;
			alu_result			: in std_logic_vector(15 downto 0);
			read_data 			: in std_logic_vector(15 downto 0);  --read data from data memory
			write_data			: in std_logic_vector(15 downto 0); -- write back to register
			clk					: in std_logic;
			stall					: in std_logic;
			--next_pc			: in std_logic; 
			
			
			mem_to_reg_out 	: out std_logic;
			alu_result_out		: out std_logic_vector(15 downto 0);
			read_data_out 		: out std_logic_vector(15 downto 0);  --read data from data memory
			write_data_out		: out std_logic_vector(15 downto 0)); -- write back to register

end MEM_WB;

architecture Behavioral of MEM_WB is
begin
	process
		begin
			if rising_edge(clk) then
				if stall = '0' then
					mem_to_reg_out <= mem_to_reg;
					alu_result_out <= alu_result;
					read_data_out <= read_data;
					write_data_out <= write_data;
				end if; 
		end if;
	end process;
end Behavioral;

