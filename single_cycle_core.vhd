---------------------------------------------------------------------------
-- single_cycle_core.vhd - A Single-Cycle Processor Implementation
--
-- Notes : 
--
-- See single_cycle_core.pdf for the block diagram of this single
-- cycle processor core.
--
-- Instruction Set Architecture (ISA) for the single-cycle-core:
--   Each instruction is 16-bit wide, with four 4-bit fields.
--
--     noop      
--        # no operation or to signal end of program
--        # format:  | opcode = 0 |  0   |  0   |   0    | 
--
--     load  rt, rs, offset     
--        # load data at memory location (rs + offset) into rt
--        # format:  | opcode = 1 |  rs  |  rt  | offset |
--
--     store rt, rs, offset
--        # store data rt into memory location (rs + offset)
--        # format:  | opcode = 3 |  rs  |  rt  | offset |
--
--     add   rd, rs, rt
--        # rd <- rs + rt
--        # format:  | opcode = 8 |  rs  |  rt  |   rd   |
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity single_cycle_core is
    port ( reset  : in  std_logic;
           clk    : in  std_logic;
			  input_out : in std_logic_vector(7 downto 0);
			  ls_addr : in std_logic_vector(3 downto 0);
			  file_flag : out std_logic);
end single_cycle_core;

architecture structural of single_cycle_core is

component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           addr_out : out std_logic_vector(4 downto 0);
			  stall	  : in std_logic );
end component;

component instruction_memory is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end component;

component sign_extend_4to16 is
    port ( data_in  : in  std_logic_vector(3 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component mux_2to1_4b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(3 downto 0);
           data_b     : in  std_logic_vector(3 downto 0);
           data_out   : out std_logic_vector(3 downto 0) );
end component;

component mux_2to1_16b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(15 downto 0);
           data_b     : in  std_logic_vector(15 downto 0);
           data_out   : out std_logic_vector(15 downto 0) );
end component;

component control_unit is
    port ( opcode     : in  std_logic_vector(3 downto 0);
           reg_dst    : out std_logic;
           reg_write  : out std_logic;
           alu_src    : out std_logic;
           mem_write  : out std_logic;
           mem_to_reg : out std_logic;
		   alu_ctr	 : out std_logic_vector(2 downto 0);
		   npc_sel	 : out std_logic_vector(1 downto 0));
end component;

component register_file is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0) );
end component;

component adder_4b is
    port ( src_a     : in  std_logic_vector(4 downto 0);
           src_b     : in  std_logic_vector(4 downto 0);
           sum       : out std_logic_vector(4 downto 0);
           carry_out : out std_logic );
end component;

component adder_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
		   alu_ctr	: in 	std_logic_vector(2 downto 0);
		   input_out : in  std_logic_vector(7 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           zero		: out std_logic );
end component;

component data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(3 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end component;

component IF_ID_reg is
	port( insn_in		: in std_logic_vector(15 downto 0);
			next_pc_in	: in std_logic_vector(4 downto 0);
			clk			: in std_logic;
			stall 		: in std_logic;
			flush 		: in std_logic;
			insn_out	   : out std_logic_vector(15 downto 0);
			next_pc_out : out std_logic_vector(4 downto 0) );
end component; 

signal sig_next_pc              : std_logic_vector(4 downto 0);
signal sig_curr_pc              : std_logic_vector(4 downto 0);
signal sig_one_4b               : std_logic_vector(4 downto 0);
signal sig_pc_carry_out         : std_logic;
signal sig_insn                 : std_logic_vector(15 downto 0);
signal sig_sign_extended_offset : std_logic_vector(15 downto 0);
signal sig_reg_dst              : std_logic;
signal sig_reg_write            : std_logic;
signal sig_alu_src              : std_logic;
signal sig_mem_write            : std_logic;
signal sig_mem_to_reg           : std_logic;
signal sig_write_register       : std_logic_vector(3 downto 0);
signal sig_write_data           : std_logic_vector(15 downto 0);
signal sig_read_data_a          : std_logic_vector(15 downto 0);
signal sig_read_data_b          : std_logic_vector(15 downto 0);
signal sig_alu_src_b            : std_logic_vector(15 downto 0);
signal sig_alu_result           : std_logic_vector(15 downto 0); 
signal sig_alu_zero        	  : std_logic;
signal sig_data_mem_out         : std_logic_vector(15 downto 0);

signal sig_alu_ctr				  : std_logic_vector(2 downto 0);
signal sig_npc_sel				  : std_logic_vector(1 downto 0);
signal sig_addi_flag				  : std_logic;
signal sig_addr_4b_sum			  : std_logic_vector(4 downto 0);
signal sig_register_dest		  : std_logic_vector(15 downto 0);
signal sig_to_write	    		  : std_logic_vector(15 downto 0);
signal sig_input_out	    		  : std_logic_vector(7 downto 0);
signal sig_file_flag				  : std_logic;
signal sig_ls_write					  : std_logic;
signal sig_mem_write_addr			  : std_logic_vector(3 downto 0);


--singal added for pipelines
signal sig_stall					  : std_logic;
signal sig_flush					  : std_logic;
signal sig_insn_out				  : std_logic_vector(15 downto 0);
signal sig_shift				  : std_logic_vector(15 downto 0);
signal sig_next_pc_out			  : std_logic_vector(4 downto 0);

signal cc_count					: std_logic_vector(15 downto 0):= (OTHERS => '0');

begin
 
	PROCESS(clk)
	BEGIN
		if clk = '1' and clk'EVENT then
			cc_count <= cc_count +1;
		END IF;
	END PROCESS;
	
	sig_stall <= '0';
    sig_one_4b <= "00001";
	
	-- used for add immediate
	sig_addi_flag <= '1' when (sig_alu_ctr = "010") else '0';
	
	-- conditions for branching/jumping
	sig_next_pc <= ('0' & sig_insn(3 downto 0)) when (((sig_npc_sel = "01") and (sig_alu_zero = '1')) or 
																			((sig_npc_sel = "11") and (sig_alu_zero = '0'))) else
						sig_insn(4 downto 0) when sig_npc_sel = "10" else 
						sig_addr_4b_sum;
	
	-- raised when obtaining next character
	file_flag <= '1' when sig_alu_ctr = "0000" else '0';
						
	 
    pc : program_counter
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_next_pc,
               addr_out => sig_curr_pc,
			   stall 	=> sig_stall); 

    next_pc : adder_4b 
    port map ( src_a     => sig_curr_pc, 
               src_b     => sig_one_4b,
               sum       => sig_addr_4b_sum,   
               carry_out => sig_pc_carry_out );
    
    insn_mem : instruction_memory 
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_curr_pc,
               insn_out => sig_insn );

    sign_extend : sign_extend_4to16 
    port map ( data_in  => sig_insn(3 downto 0),
               data_out => sig_sign_extended_offset );

    ctrl_unit : control_unit 
    port map ( opcode     => sig_insn(15 downto 12),
               reg_dst    => sig_reg_dst,
               reg_write  => sig_reg_write,
               alu_src    => sig_alu_src,
               mem_write  => sig_mem_write,
               mem_to_reg => sig_mem_to_reg,
					alu_ctr	  => sig_alu_ctr,
					npc_sel	  => sig_npc_sel);

    mux_reg_dst : mux_2to1_4b 
    port map ( mux_select => sig_reg_dst,
               data_a     => sig_insn(7 downto 4),
               data_b     => sig_insn(3 downto 0),
               data_out   => sig_write_register );

    reg_file : register_file 
    port map ( reset           => reset, 
               clk             => clk,
               read_register_a => sig_insn(11 downto 8),
               read_register_b => sig_insn(7 downto 4),
               write_enable    => sig_reg_write,
               write_register  => sig_write_register,
               write_data      => sig_write_data,
               read_data_a     => sig_read_data_a,
               read_data_b     => sig_read_data_b );
    
    mux_alu_src : mux_2to1_16b 
    port map ( mux_select => sig_alu_src,
               data_a     => sig_read_data_b,
               data_b     => sig_sign_extended_offset,
               data_out   => sig_alu_src_b );
   

    -- amount to shift by, immediate value
    sig_shift <= "000000000000" & sig_insn(3 downto 0) when sig_alu_ctr = "100" else sig_alu_src_b;
	
    alu : adder_16b 
    port map ( src_a     => sig_read_data_a,
               src_b     => sig_shift,
			   alu_ctr	 => sig_alu_ctr,
			   input_out => input_out,
               sum       => sig_alu_result,
               zero => sig_alu_zero );

	-- used to write into memory when LS is being executed
	sig_mem_write_addr <= ls_addr when (sig_mem_write = '1' AND sig_reg_write = '1' )else sig_alu_result(3 downto 0);
	
    data_mem : data_memory 
    port map ( reset        => reset,
               clk          => clk,
               write_enable => sig_mem_write,
               write_data   => sig_read_data_b,
               addr_in      => sig_mem_write_addr,
               data_out     => sig_data_mem_out );
               
    mux_mem_to_reg : mux_2to1_16b 
    port map ( mux_select => sig_mem_to_reg,
               data_a     => sig_alu_result,
               data_b     => sig_data_mem_out,
               data_out   => sig_write_data );

end structural;
