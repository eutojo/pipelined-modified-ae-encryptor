----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:24:34 03/29/2018 
-- Design Name: 
-- Module Name:    rn_table - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rn_table is
	port(to_encode : in std_logic_vector(7 downto 0);
		  encrypt_key : in std_logic_vector(7 downto 0);
		  encoded	 : out std_logic_vector(15 downto 0));
end rn_table;

architecture Behavioral of rn_table is
	signal xor_result: std_logic_vector(7 downto 0);
	signal encryption_addr: std_logic_vector(3 downto 0);
begin	
	xor_result <= to_encode XOR encrypt_key;
	encryption_addr <= xor_result(3 downto 0);
	
	encoded <= (X"0011") when encryption_addr = X"0" else
				  (X"0090") when encryption_addr = X"1" else
				  (X"0052") when encryption_addr = X"2" else
				  (X"00C8") when encryption_addr = X"3" else
				  (X"00B7") when encryption_addr = X"4" else
				  (X"00CE") when encryption_addr = X"5" else
				  (X"00D4") when encryption_addr = X"6" else
				  (X"0031") when encryption_addr = X"7" else
				  (X"00D3") when encryption_addr = X"8" else
				  (X"00CB") when encryption_addr = X"9" else
				  (X"00F1") when encryption_addr = X"A" else
				  (X"00B5") when encryption_addr = X"B" else
				  (X"0073") when encryption_addr = X"C" else
				  (X"00AB") when encryption_addr = X"D" else
				  (X"00BF") when encryption_addr = X"E" else
				  (X"0062");

end Behavioral;

