----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:40:42 04/22/2018 
-- Design Name: 
-- Module Name:    EX_MEM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EX_MEM is
	port( mem_to_reg	 		: in std_logic;
			mem_write			: in std_logic;
			read_data_b			: in std_logic_vector(15 downto 0);
			reg_write			: in std_logic_vector(15 downto 0); -- write back to register
			alu_result			: in std_logic_vector(15 downto 0);
			zero					: in std_logic;
			clk					: in std_logic;
			stall					: in std_logic;
			--next_pc			: in std_logic;
			
			mem_to_reg_out	 	: in std_logic;
			mem_write_out		: in std_logic;
			read_data_b_out	: in std_logic_vector(15 downto 0);
			reg_write_out		: in std_logic_vector(15 downto 0);
			alu_result_out		: in std_logic_vector(15 downto 0));
			
end EX_MEM;

architecture Behavioral of EX_MEM is
	
begin
	process
		begin
			if rising_edge(clk) then
				if stall = '0' then
					mem_to_reg_out <= mem_to_reg;
					mem_write_out <= mem_write;
					read_data_b_out <= read_data_b;
					reg_write_out <= reg_write;
					alu_result_out <= alu_result;
				end if;
			end if;
		end process;
end Behavioral;

