---------------------------------------------------------------------------
-- instruction_memory.vhd - Implementation of A Single-Port, 16 x 16-bit
--                          Instruction Memory.
-- 
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity instruction_memory is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(4 downto 0);
           insn_out : out std_logic_vector(15 downto 0) );
end instruction_memory;

architecture behavioral of instruction_memory is

type mem_array is array(0 to 31) of std_logic_vector(15 downto 0);
signal sig_insn_mem : mem_array;

begin
    mem_process: process ( clk,
                           addr_in ) is
  
    variable var_insn_mem : mem_array;
    variable var_addr     : integer;
  
    begin
        if (reset = '1') then
			
				-- load the encryption key into the registers
				var_insn_mem(0)  := X"1010"; -- $1 <= 1234
            var_insn_mem(1)  := X"1021"; -- $2 <= 5678
            var_insn_mem(2)  := X"1032"; -- $3 <= 9ABC
            var_insn_mem(3)  := X"1043"; -- $4 <= DEF0
				
				-- load the last AND to ensure that the string is printable
            var_insn_mem(4)  := X"1054"; -- $5 <= 0x7F
				
				-- load in for EOF comparison
				var_insn_mem(5)  := X"1065"; -- $6 <= "x80"
				
				var_insn_mem(6)  := X"10F0"; -- $15 <= tag gen key
				
				-- character encryption
				var_insn_mem(7)  := X"D088"; -- $8 <= character to encrypt
				var_insn_mem(8)  := X"F80C"; -- if not EOF, go to 10
				var_insn_mem(9)  := X"0000"; -- branch delay
				var_insn_mem(10) := X"7019"; -- jump to the end
				var_insn_mem(11) := X"0000"; 
				var_insn_mem(12) := X"E818"; -- 8 XOR 1234
				var_insn_mem(13) := X"E828"; -- 8 XOR 5678
				var_insn_mem(14) := X"E838"; -- 8 XOR 9ABC
				var_insn_mem(15) := X"E848"; -- 8 XOR DEF0				
				var_insn_mem(16) := X"B858"; -- the last AND
				
				-- tag generation
				var_insn_mem(17) := X"6F9F"; -- get the MSB off $15 and into $9
				var_insn_mem(18) := X"C89B"; -- shift encrypted according to $9 and store in B
				var_insn_mem(19) := X"EBAA"; -- XOR $11 with $10

				-- shifting the key
				var_insn_mem(20) := X"5FB1"; -- shift the key left (results in B)
				var_insn_mem(21) := X"0000"; -- shift the key right once (result in $15)
				var_insn_mem(22) := X"AB9F"; -- OR the two and store into $15 (key wrapped)
				
				-- get next character to encrypt
				var_insn_mem(23) := X"7007"; 
				
				-- get next character to encrypt
				var_insn_mem(24) := X"0000";
				var_insn_mem(25) := X"38A8";
				
				-- store tag in memory
				var_insn_mem(26) := X"0000";
				var_insn_mem(27) := X"0000";
				var_insn_mem(28) := X"701B";
				var_insn_mem(29) := X"0000";
				var_insn_mem(30) := X"0000";
				var_insn_mem(31) := X"0000";
        
        elsif (rising_edge(clk)) then
            -- read instructions on the rising clock edge
            var_addr := conv_integer(addr_in);
            insn_out <= var_insn_mem(var_addr);
        end if;

        -- the following are probe signals (for simulation purpose)
        sig_insn_mem <= var_insn_mem;

    end process;
  
end behavioral;
