----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:06:51 04/24/2018 
-- Design Name: 
-- Module Name:    hazard_detection_unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hazard_detection_unit is
	port( mem_read_EX : in std_logic; -- ID/EX.MEMRead
			reg_Rt_EX	: in std_logic; -- ID/EX.RegisterRt
			reg_Rs_ID	: in std_logic; -- IF/ID.RegisterRs
			reg_Rt_EX	: in std_logic; -- ID/EX.RegisterRt
			reg_Rt_ID	: in std_logic; -- IF/ID.RegisterRt
			stall			: out std_logic );
end hazard_detection_unit;

architecture Behavioral of hazard_detection_unit is

begin
	hazard_detect: process(mem_read_EX,
								  reg_Rt_EX,
								  reg_Rs_ID,
								  reg_Rt_EX,
								  reg_Rt_ID)
	begin
		stall <= '0'; -- initialise stall
		
		--check for hazard
		if(	 mem_read_EX 
				 and ( (reg_Rt_EX = reg_Rs_ID) or (reg_Rt_EX = reg_Rt_ID) ) ) then -- hazard detected
			stall <= '1';
		end if;
	end process;

end Behavioral;

