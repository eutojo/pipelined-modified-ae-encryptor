----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:50:20 04/24/2018 
-- Design Name: 
-- Module Name:    forwarding_unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity forwarding_unit is
	port( reg_Rs_EX		: in std_logic_vector(3 downto 0); -- ID/EX.RegisterRs
			reg_Rt_EX		: in std_logic_vector(3 downto 0); -- ID/EX.RegisterRt
			reg_Rd_MEM		: in std_logic_vector(3 downto 0); -- EX/MEM.RegisterRd
			reg_write_MEM	: in std_logic; -- EX/MEM.RegWrite
			reg_Rd_WB		: in std_logic_vector(3 downto 0); -- MEM/EX.RegisterRd
			reg_write_WB	: in std_logic; -- MEM/WB.RegWrite
			
			forward_a		: out std_logic_vector(1 downto 0) ;
			forward_b		: out std_logic_vector(1 downto 0) );
end forwarding_unit;

architecture Behavioral of forwarding_unit is
begin
	
	forwarding_logic: process( reg_Rs_EX,
										reg_Rt_EX,
										reg_Rd_MEM,
										reg_write_MEM,
										reg_Rd_WB,
										reg_write_WB )
									
	begin
		------ EX hazard ---------------------
		if( 	reg_write_MEM
				and (reg_Rd_MEM /= "0000") 
				and (reg_Rd_MEM = reg_RS_EX) ) then
			forward_A <= "10";
		elsif( reg_write_MEM 
				 and (reg_Rd_MEM /= "0000")
				 and (reg_rd_MEM = reg_Rt_EX) ) then
			forward_B <= "10";
		
		------ MEM hazard ---------------------
		elsif( reg_write_WB and (reg_Rd_WB /= "0000") 
				 and not (reg_write_MEM and (reg_Rd_MEM /= "0000") and (reg_Rd_MEM /= reg_Rs_EX))
				 and (reg_Rd_WB = reg_Rs_EX) ) then
			forward_A <= "01";
		elsif( reg_write_WB 
				 and (reg_Rd_WB /= "0000")
				 and not (reg_write_MEM and (reg_Rd_MEM /= "0000"))
				 and (reg_Rd_MEM = reg_Rt_EX) ) then
			forward_B <= "01";
		end if;	
		
	end process;
	
end Behavioral;

