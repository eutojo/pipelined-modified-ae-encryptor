----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:05:55 04/24/2018 
-- Design Name: 
-- Module Name:    shift_left_2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_left_2 is
	port( data_in	: in std_logic_vector(15 downto 0);
			data_out	: out std_logic_vector(15 downto 0) );
end shift_left_2;

architecture Behavioral of shift_left_2 is

signal sig_data_in : std_logic_vector(15 downto 0);

begin
	shift_left_2 : process(data_in) 
	begin
		sig_data_in <= data_in sll 2;
		data_out <= sig_data_in;
	end process;

end Behavioral;

