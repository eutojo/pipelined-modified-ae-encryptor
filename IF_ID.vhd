----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:27:31 04/20/2018 
-- Design Name: 
-- Module Name:    IF_ID - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IF_ID_reg is
	port( insn_in		: in std_logic_vector(15 downto 0);
			next_pc_in	: in std_logic_vector(4 downto 0);
			clk			: in std_logic;
			stall 		: in std_logic;
			flush 		: in std_logic;
			insn_out	   : out std_logic_vector(15 downto 0);
			next_pc_out : out std_logic_vector(4 downto 0) );

end IF_ID_reg;

architecture Behavioral of IF_ID_reg is

begin
	
    update_process: process ( clk ) is
    begin
      if (rising_edge(clk)) then 
           if (flush = '1') then
					insn_out <= (others => '0'); 
			  elsif (stall = '0') then
					insn_out <= insn_in;
					next_pc_out <= next_pc_in;
			  else
					-- stall
			  end if;
		end if;
    end process;

end Behavioral;

